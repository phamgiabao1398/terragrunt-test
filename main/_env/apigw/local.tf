locals {
  OrganizationNameAbbreviation = "mcredit"
  OwnerDepartment              = "Dev/sysops"
  ResourceDepartmentCreator    = "sysops"
  account                      = var.aws_account_2
  environment                  = var.environment
  project                      = var.project
  
  endpoint_type = var.endpoint_type

  public_policy = templatefile("apigw-config/policy-public.json",
    {
      account_id = local.account_id
    }
  )

  account_id     = data.aws_caller_identity.this.account_id
}

data "aws_caller_identity" "this" {}
