module "api_gateway" {
  source = "D:/Mcredit/gitlab/aws-apigw"
  
  openapi_config = templatefile("apigw-config/demo-apigw.yaml",
    {
      env = local.environment
    }
  )

  environment = local.environment
  account     = local.account
  project     = local.project
  name_prefix = "apigw"

  endpoint_type                = local.endpoint_type
  disable_execute_api_endpoint = false
  description                  = "APIGW for ${local.project} ${local.environment}"
  logging_level                = "OFF"
  xray_tracing_enabled         = false
  retention_in_days            = 7
  stage_variables = {
    "vpclink" = var.vpclink
    "domain"  = var.api_backend_domain
  }
}