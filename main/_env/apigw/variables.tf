variable "region" {
  description = "The name of the AWS Region"
  type        = string
  # default       = "ap-southeast-1"
}

variable "ResourceCreator" {
  type    = string
  default = ""
}

variable "environment" {
  type    = string
  default = ""
}

variable "aws_account_1" {
  type    = string
  default = ""
}
variable "aws_account_2" {
  type    = string
  default = ""
}
variable "apigw_cert_arn" {
  type    = string
  default = ""
}

variable "vpclink" {
  type    = string
  default = ""
}
variable "vpc_endpoint_ids" {
  type    = string
  default = ""
}
variable "project" {
  type    = string
  default = ""
}
variable "endpoint_type" {
  type    = string
  default = ""
}
variable "api_backend_path" {
  type    = string
  default = ""
}
variable "api_backend_domain" {
  type    = string
  default = ""
}
