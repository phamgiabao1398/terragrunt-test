# -----------------------------------------------------------------------------
# TERRAGRUNT CONFIGURATION
# -----------------------------------------------------------------------------

locals {
    #common_vars  = read_terragrunt_config("${get_terragrunt_dir()}/common.hcl")
    project_folder = "test"
    provider_vars  = read_terragrunt_config(find_in_parent_folders("locals.hcl"))
    region  = local.provider_vars.inputs.region
    profile = local.provider_vars.inputs.profile
}

# -----------------------------------------------------------------------------
# GENERATED PROVIDER BLOCK
# -----------------------------------------------------------------------------

generate "provider" {
    path = "provider.tf"
    if_exists = "overwrite_terragrunt"
    contents = <<EOF
provider "aws" {
    # The AWS region in which all resources will be created
    region = "${local.region}"
    profile = "${local.profile}"
    #version = ">= 4.0"
}
EOF
}