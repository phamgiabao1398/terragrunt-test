locals {
  env_folder = "dev"
}

inputs = {
  region  = "ap-southeast-1"                  # Need to config only if you want multi reigions
  profile = yamldecode(file(find_in_parent_folders("config.yaml")))[yamldecode(local.env_folder)]
  environment     = "dev"
}