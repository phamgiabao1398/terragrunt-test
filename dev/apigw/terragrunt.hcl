include "root" {
  path = find_in_parent_folders()
}

include "vars" {
  path = "../locals.hcl"
  merge_strategy = "deep"
}

terraform {
  source = "../../main/_env/apigw"
}

inputs = {
  aws_account_2 = "sandbox"
  project = "demo-apigw"
  endpoint_type = "REGIONAL"
  vpclink = "dmhssn"
  api_backend_domain = "sandbox.mcredit.com.vn"
}
